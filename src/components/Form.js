import styled from "styled-components";

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    width: 600px;
    margin: 10px auto;
`;