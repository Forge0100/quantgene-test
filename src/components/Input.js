import styled from "styled-components";

export const Input = styled.input`
order: 1px solid #000;
    padding: 18.5px 14px;
    margin: 5px 0;
    box-sizing: border-box;
`;