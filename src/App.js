import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import { Store, useStore } from './store';
import { Home } from './pages/Home';
import { Login } from "./pages/Login";
import { SetPassword } from "./pages/SetPassword";

function App() {
  const storeValue = useStore();

  return (
    <Store.Provider value={storeValue}>
      <Router>
        <Switch>
            <Route path="/activate-code/:code">
              <SetPassword />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
      </Router>
    </Store.Provider>
  );
}

export default App;
