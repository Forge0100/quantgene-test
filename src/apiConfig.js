const { REACT_APP_API_URL: api } = process.env;

export const apiConfig = {
    activeCode: (code) => `${api}/activate-code/${code}`,
    setPassword: `${api}/set-password`,
    login: `${api}/login`,
    me: `${api}/users/me`,
};