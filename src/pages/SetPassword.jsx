import React, { useState, useCallback, useRef, useEffect } from 'react';
import  { useParams, useHistory } from 'react-router-dom';

import { Box } from '../components/Box';
import { Form } from '../components/Form';
import { Input } from '../components/Input';
import { apiConfig } from '../apiConfig';

export const SetPassword = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { code } = useParams();
    const history = useHistory();
    const passwordInputRef = useRef();

    const handleChange = ({ target }) => {
        setPassword(target.value);
    };

    const handleSubmit = useCallback((event) => {
        event.preventDefault();
        
        const data = { code, password };

        fetch(apiConfig.setPassword, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(() => {
            history.push({ pathname: '/login', state: { email } });
        });
    }, [history, code, email, password]);

    useEffect(() => {
        fetch(apiConfig.activeCode(code))
        .then(response => response.json())
        .then(({ data }) => {
            setEmail(data.email);
        });
    }, [setEmail, code]);

    useEffect(() => {
        passwordInputRef.current.focus();
    }, []);

    return (
        <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
            <h1>Set password:</h1>
            <Form onSubmit={handleSubmit}>
                <Input type="email" value={email} disabled />
                <Input 
                    type="password" 
                    placeholder="Password"
                    value={password} 
                    onChange={handleChange} 
                    ref={passwordInputRef}
                    required
                />
                <Input type="submit" value="Change" />
            </Form>
        </Box>
    );
};
