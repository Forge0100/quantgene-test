import React, { useContext, useEffect } from 'react';
import  { Redirect } from 'react-router-dom';

import { Store } from '../store';
import { apiConfig } from '../apiConfig';

export const Home = () => {
    const { state, dispatch } = useContext(Store);
    const token = localStorage.getItem("token");

    useEffect(() => {
        if (!token) {
            return;
        }

        fetch(apiConfig.me, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(({ data }) => {
            dispatch({ type: 'SET_USER', payload: data });
        });
    }, [dispatch, token]);

    if (!token) {
        return <Redirect to="/login" />
    }

    const { user } = state;

    return <h1>Welcome, {user.name}!</h1>;
};
