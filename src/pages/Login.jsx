import React, { useContext, useEffect, useRef, useState } from 'react';
import  { useLocation, useHistory } from 'react-router-dom';

import { Store } from '../store';
import { apiConfig } from '../apiConfig';
import { Box } from '../components/Box';
import { Form } from '../components/Form';
import { Input } from '../components/Input';

export const Login = () => {
    const location = useLocation();
    const history = useHistory();
    const { dispatch } = useContext(Store);

    const { email: defaultEmail = '' } = location.state || {};

    const [email, setEmail] = useState(defaultEmail);
    const [password, setPassword] = useState('');
    
    const loginInputRef = useRef();
    const passwordInputRef = useRef();

    const handleSubmit = (event) => {
        event.preventDefault();

        const data = { email, password };

        fetch(apiConfig.login, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
        .then(({ data }) => {
            const { name } = data;
            const payload = { email, name };

            localStorage.setItem("token", data.token);
            dispatch({ type: 'SET_USER', payload });
            
            history.push('/');
        });
    };

    const onChange = (handler) => ({ target }) => {
        handler(target.value);
    };

    useEffect(() => {
        const inputRef = defaultEmail ? passwordInputRef : loginInputRef;

        inputRef.current.focus();
    }, [defaultEmail]);

    return (
        <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
            <h1>Sign in:</h1>
            <Form onSubmit={handleSubmit}>
                <Input 
                    type="email" 
                    placeholder="Email" 
                    value={email} 
                    onChange={onChange(setEmail)}
                    ref={loginInputRef}
                />
                <Input 
                    type="password" 
                    placeholder="Password" 
                    value={password} 
                    onChange={onChange(setPassword)}
                    ref={passwordInputRef}
                />
                <Input type="submit" value="Sign in" />
            </Form>
        </Box>
    );
};
