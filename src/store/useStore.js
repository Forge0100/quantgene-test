import { useReducer } from 'react'

import { initialState } from './initialState';
import { reducer } from './reducer';

export const useStore = () => {
    const [state, dispatch] = useReducer(reducer, initialState);

    return { state, dispatch };
};